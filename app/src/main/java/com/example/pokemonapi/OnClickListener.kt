package com.example.pokemonapi

import com.example.pokemonapi.infoPokemon.Result

interface OnClickListener {
    fun onClick(pokemon: Result)
}