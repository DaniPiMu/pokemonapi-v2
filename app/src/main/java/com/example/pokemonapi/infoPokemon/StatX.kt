package com.example.pokemonapi.infoPokemon

data class StatX(
    val name: String,
    val url: String
)