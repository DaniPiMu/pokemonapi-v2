package com.example.pokemonapi.infoPokemon

data class Types(
    val name: String,
    val url: String
)