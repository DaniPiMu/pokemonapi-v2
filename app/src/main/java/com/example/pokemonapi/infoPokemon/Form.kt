package com.example.pokemonapi.infoPokemon

data class Form(
    val name: String,
    val url: String
)