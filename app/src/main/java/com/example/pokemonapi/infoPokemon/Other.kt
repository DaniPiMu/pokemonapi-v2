package com.example.pokemonapi.infoPokemon

import com.google.gson.annotations.SerializedName

data class Other(
    val dream_world: DreamWorld,
    val home: Home,
    @SerializedName("official_artwork")
    val officialartwork: OfficialArtwork
)