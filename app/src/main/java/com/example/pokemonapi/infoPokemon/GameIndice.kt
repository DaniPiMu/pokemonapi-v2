package com.example.pokemonapi.infoPokemon

data class GameIndice(
    val game_index: Int,
    val version: Version
)