package com.example.pokemonapi.infoPokemon

data class VersionX(
    val name: String,
    val url: String
)