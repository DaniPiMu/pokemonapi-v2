package com.example.pokemonapi.infoPokemon

import android.os.Parcelable

data class Result(
    val name: String,
    val url: String

)
