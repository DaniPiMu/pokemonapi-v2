package com.example.pokemonapi.infoPokemon

data class OfficialArtwork(
    val front_default: String
)