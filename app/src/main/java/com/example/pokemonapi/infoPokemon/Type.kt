package com.example.pokemonapi.infoPokemon

data class Type(
    val slot: Int,
    val type: Types
)