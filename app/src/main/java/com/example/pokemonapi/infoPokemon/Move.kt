package com.example.pokemonapi.infoPokemon

data class Move(
    val move: MoveX,
    val version_group_details: List<VersionGroupDetail>
)