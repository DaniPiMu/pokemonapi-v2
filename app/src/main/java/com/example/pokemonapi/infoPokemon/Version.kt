package com.example.pokemonapi.infoPokemon

data class Version(
    val name: String,
    val url: String
)