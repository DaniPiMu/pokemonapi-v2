package com.example.pokemonapi.infoPokemon

data class HeldItem(
    val item: Item,
    val version_details: List<VersionDetail>
)