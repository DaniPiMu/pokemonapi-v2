package com.example.pokemonapi.infoPokemon

data class MoveX(
    val name: String,
    val url: String
)