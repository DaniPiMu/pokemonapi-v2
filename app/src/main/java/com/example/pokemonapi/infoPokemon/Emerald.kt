package com.example.pokemonapi.infoPokemon

data class Emerald(
    val front_default: String,
    val front_shiny: String
)