package com.example.pokemonapi.infoPokemon

data class MoveLearnMethod(
    val name: String,
    val url: String
)