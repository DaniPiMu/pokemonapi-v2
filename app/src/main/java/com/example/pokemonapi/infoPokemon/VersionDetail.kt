package com.example.pokemonapi.infoPokemon

data class VersionDetail(
    val rarity: Int,
    val version: VersionX
)