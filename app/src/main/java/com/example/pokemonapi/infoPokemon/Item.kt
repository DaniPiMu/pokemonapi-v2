package com.example.pokemonapi.infoPokemon

data class Item(
    val name: String,
    val url: String
)