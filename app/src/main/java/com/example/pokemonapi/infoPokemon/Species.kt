package com.example.pokemonapi.infoPokemon

data class Species(
    val name: String,
    val url: String
)