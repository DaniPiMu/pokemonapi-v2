package com.example.pokemonapi.infoPokemon

data class Stat(
    val base_stat: Int,
    val effort: Int,
    val stat: StatX
)