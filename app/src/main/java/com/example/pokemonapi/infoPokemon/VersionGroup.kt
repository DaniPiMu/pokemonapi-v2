package com.example.pokemonapi.infoPokemon

data class VersionGroup(
    val name: String,
    val url: String
)