package com.example.pokemonapi.infoPokemon

data class AbilityX(
    val name: String,
    val url: String
)