package com.example.pokemonapi.adapter
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.example.pokemonapi.R
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.pokemonapi.databinding.ItemPokemonBinding

import com.example.pokemonapi.OnClickListener
import com.example.pokemonapi.Repository
import com.example.pokemonapi.infoPokemon.PokeApi
import com.example.pokemonapi.infoPokemon.Result
import com.example.pokemonapi.infoPokemon.Type
import com.example.pokemonapi.model.PokemonViewModel

class UserAdapter(private val pokemons: List<Result>, private val listener: OnClickListener):


    RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemPokemonBinding.bind(view);

        fun setListener(pokemon: Result){
            binding.root.setOnClickListener {
                listener.onClick(pokemon)
            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_pokemon, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pokemon = pokemons[position]
        with(holder){
            setListener(pokemon)
            binding.namePokemon.text = pokemon.name
            val url = pokemon.url
            val index = url.split("/".toRegex()).dropLast(1).last()
            val imageUrl =  "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/$index.png"
            Glide.with(context)
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.imgPokemon)

//            val repository = Repository()
//            val actualPokeApi = repository.fetchDataPokemon(url)
//            val pokemonTypes: List<Type> = actualPokeApi.value!!.types
//
//            when(pokemonTypes[0].type.name){
//                "fire"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.fire))
//                "normal"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.normal))
//                "fighting"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.fighting))
//                "flying"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.flying))
//                "poison"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.poison))
//                "ground"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.ground))
//                "rock"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.rock))
//                "bug"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.bug))
//                "ghost"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.ghost))
//                "steel"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.steel))
//                "water"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.water))
//                "grass"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.grass))
//                "electric"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.electric))
//                "psychic"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.psychic))
//                "ice"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.ice))
//                "dragon"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.dragon))
//                "dark"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.dark))
//                "fairy"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.fairy))
//                "unknown"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.unknown))
//                "shadow"-> holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.shadow))
//            }
        }

}
    override fun getItemCount(): Int {
        return pokemons.size
    }

}
