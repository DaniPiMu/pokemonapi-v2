package com.example.pokemonapi.Room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(PokemonEntity::class), version = 1)
abstract class PokemonDatabase: RoomDatabase() {
    abstract fun PokemonDAO(): PokemonDAO
}
