package com.example.pokemonapi.Room

import androidx.room.*

@Dao
interface PokemonDAO {
    @Query("SELECT * FROM PokemonEntity")
    fun getAllContacts(): MutableList<PokemonEntity>
    @Insert
    fun addContact(contactEntity: PokemonEntity)
    @Update
    fun updateContact(contactEntity: PokemonEntity)
    @Delete
    fun deleteContact(contactEntity: PokemonEntity)
}
