package com.example.pokemonapi.Room

import android.app.Application
import androidx.room.Room

class PokemonAplication: Application() {
    companion object {
        lateinit var database: PokemonDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            PokemonDatabase::class.java,
            "ContactDatabase").build()
    }
}
