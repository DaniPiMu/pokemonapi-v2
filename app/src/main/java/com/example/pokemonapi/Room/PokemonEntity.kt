package com.example.pokemonapi.Room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "PokemonEntity")
data class PokemonEntity(
    @PrimaryKey(autoGenerate = true)
    var pokedexNum: Long,
    var img: String,
    var name:String,
    var kg: Int,
    var weight:Int,
    var high: Int,
    var hp: Int,
    var atk: Int,
    var def: Int,
    var especialAtk: Int,
    var especialDef: Int,
    var spd: Int)

