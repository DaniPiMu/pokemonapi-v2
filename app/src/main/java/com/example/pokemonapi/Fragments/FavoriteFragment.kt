package com.example.pokemonapi.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pokemonapi.OnClickListener
import com.example.pokemonapi.R
import com.example.pokemonapi.adapter.UserAdapter
import com.example.pokemonapi.databinding.FragmentFavoriteBinding
import com.example.pokemonapi.databinding.FragmentPokemonListBinding
import com.example.pokemonapi.infoPokemon.Data
import com.example.pokemonapi.infoPokemon.Result
import com.example.pokemonapi.model.PokemonViewModel

class FavoriteFragment : Fragment() ,OnClickListener{
    private lateinit var userAdapter: UserAdapter
    private lateinit var binding: FragmentFavoriteBinding

    private val model: PokemonViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoriteBinding.inflate(inflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.data.value?.let {
            setUpRecyclerView(it)
        }
        model.data.observe(viewLifecycleOwner, Observer{
            if (model.data.value ==null){
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            }
            else{
                if (it != null){
                    setUpRecyclerView(it)
                }
            }
        })
    }

    override fun onClick(pokemon: Result) {
        model.selectedPokemon(pokemon)
        view?.let {
            Navigation.findNavController(it).navigate(R.id.action_pokemonList_to_detailPokemon)
        }
    }
    private fun setUpRecyclerView(myData: Data) {
        userAdapter = UserAdapter(myData.results, this)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = LinearLayoutManager(context)
            adapter = userAdapter
        }
    }
}