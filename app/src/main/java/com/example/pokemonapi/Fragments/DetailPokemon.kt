package com.example.pokemonapi.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.pokemonapi.R
import com.example.pokemonapi.databinding.FragmentDetailPokemonBinding
import com.example.pokemonapi.infoPokemon.PokeApi
import com.example.pokemonapi.model.PokemonViewModel

class DetailPokemon : Fragment() {

    private lateinit var binding: FragmentDetailPokemonBinding
    private val model: PokemonViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentDetailPokemonBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val imgBind = binding.cardPokemon
        super.onViewCreated(view, savedInstanceState)

        model.actualPokeApi.observe(viewLifecycleOwner, Observer {
            val pokemon = it
            Glide.with(requireContext())
                .load(pokemon!!.sprites.other.home.front_default)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.imgPokemon)
            binding.pokemonName.text = pokemon.name
            val peso :Double = pokemon.weight.toDouble()/10
            binding.kg.text = peso.toString() + " KG"
            var altura :Double = pokemon.height.toDouble()/10
            binding.metros.text = altura.toString() + " M"
            binding.hpProgressBar.progress = pokemon.stats[0].base_stat
            binding.atkProgressBar.progress = pokemon.stats[1].base_stat
            binding.defProgressBar.progress = pokemon.stats[2].base_stat
            binding.especialAtkProgressBar.progress = pokemon.stats[3].base_stat
            binding.especialDefProgressBar.progress = pokemon.stats[4].base_stat
            binding.spdProgressBar.progress = pokemon.stats[5].base_stat
            binding.pokedexNum.text = "#"+pokemon.id

            when(pokemon.types[0].type.name){
                "fire"-> imgBind.setBackgroundColor(resources.getColor(R.color.fire))
                "normal"-> imgBind.setBackgroundColor(resources.getColor(R.color.normal))
                "fighting"-> imgBind.setBackgroundColor(resources.getColor(R.color.fighting))
                "flying"-> imgBind.setBackgroundColor(resources.getColor(R.color.flying))
                "poison"-> imgBind.setBackgroundColor(resources.getColor(R.color.poison))
                "ground"-> imgBind.setBackgroundColor(resources.getColor(R.color.ground))
                "rock"-> imgBind.setBackgroundColor(resources.getColor(R.color.rock))
                "bug"-> imgBind.setBackgroundColor(resources.getColor(R.color.bug))
                "ghost"-> imgBind.setBackgroundColor(resources.getColor(R.color.ghost))
                "steel"-> imgBind.setBackgroundColor(resources.getColor(R.color.steel))
                "water"-> imgBind.setBackgroundColor(resources.getColor(R.color.water))
                "grass"-> imgBind.setBackgroundColor(resources.getColor(R.color.grass))
                "electric"-> imgBind.setBackgroundColor(resources.getColor(R.color.electric))
                "psychic"-> imgBind.setBackgroundColor(resources.getColor(R.color.psychic))
                "ice"-> imgBind.setBackgroundColor(resources.getColor(R.color.ice))
                "dragon"-> imgBind.setBackgroundColor(resources.getColor(R.color.dragon))
                "dark"-> imgBind.setBackgroundColor(resources.getColor(R.color.dark))
                "fairy"-> imgBind.setBackgroundColor(resources.getColor(R.color.fairy))
                "unknown"-> imgBind.setBackgroundColor(resources.getColor(R.color.unknown))
                "shadow"-> imgBind.setBackgroundColor(resources.getColor(R.color.shadow))
            }
        })
    }


}