package com.example.pokemonapi

import com.example.pokemonapi.Room.PokemonEntity
import com.example.pokemonapi.infoPokemon.PokeApi

data class PokemonGeneral(
    val id: Long,
    val name: String,
    val weight: Int,
    val height: Int,
    var image: String,
    var hp: Int,
    var atk: Int,
    var def: Int,
    var sAtk: Int,
    var sDef:Int,
    var spd:Int)

fun PokeApi.toPokemonGeneral()= PokemonGeneral(id.toLong(),name,weight,height,sprites.front_default,
    stats[0].base_stat,stats[1].base_stat,stats[2].base_stat,stats[3].base_stat,stats[4].base_stat,stats[5].base_stat)

fun PokemonEntity.toPokemonGeneral()= PokemonGeneral(pokedexNum,name, weight,high, img,hp, atk, def, especialAtk, especialDef,spd)