package com.example.pokemonapi

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.pokemonapi.infoPokemon.Data
import com.example.pokemonapi.infoPokemon.PokeApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.example.pokemonapi.retroFit.ApiInterficie

class Repository {

    private val apiInterface = ApiInterficie.create()
    var dataInfo = MutableLiveData<Data?>()
    var pokemonInfo = MutableLiveData<PokeApi?>()


    fun fetchData(url: String): MutableLiveData<Data?> {
        val call = apiInterface.getData(url)

        call.enqueue(object: Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                dataInfo.postValue(null)
            }

            override fun onResponse(call: Call<Data?>, response: Response<Data?>) {
                if (response != null && response.isSuccessful) {
                    dataInfo.value = response.body()
                }
            }
        })
        return dataInfo
    }



    suspend fun fetchDataPokemon(url: String): PokeApi? {
        val response = apiInterface.getPokemon(url)
        if(response.isSuccessful){
            return response.body()
        }
        else{
            return null
        }
    }
}

