package com.example.pokemonapi.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemonapi.Repository
import com.example.pokemonapi.infoPokemon.Data
import com.example.pokemonapi.infoPokemon.PokeApi
import com.example.pokemonapi.infoPokemon.Result
import kotlinx.coroutines.*

class PokemonViewModel: ViewModel() {
    val repository = Repository()
    var data = MutableLiveData<Data?>()
    var actualCharacter = MutableLiveData<Result>()
    var actualPokeApi = MutableLiveData<PokeApi?>()

    init {
        fetchData("pokemon?limit=1118")
    }

    fun fetchData(url: String){
        data = repository.fetchData(url)

    }

    fun fetchDataPokemon(url: String){
        //actualPokeApi = repository.fetchDataPokemon(url)
    }

    fun selectedPokemon(character: Result){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){ repository.fetchDataPokemon(character.url) }
            actualPokeApi.postValue(response)
        }
    }

}