package com.example.pokemonapi.retroFit

import com.example.pokemonapi.infoPokemon.Data
import com.example.pokemonapi.infoPokemon.PokeApi
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterficie {
    @GET()
    fun getData(@Url url: String): Call<Data>

    @GET()
    suspend fun getPokemon(@Url url: String): Response<PokeApi>

    companion object {
        val BASE_URL = "https://pokeapi.co/api/v2/"

        fun create(): ApiInterficie {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterficie::class.java)
        }
    }
}